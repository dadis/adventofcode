package sk.bado.advent.day06

import scala.io.Source

/**
  * Created by DAVID on 6.12.2016.
  */
object App06 {

  def readInputFile(): List[String] = {

    Source.fromFile("./input06.txt")
      .getLines
      .toList
  }


  def flip(input: List[String]): List[String] = {

    if (input.head.isEmpty) {
      return List()
    }
    input.map(list => list.head).mkString :: flip(input.map(list => list.tail))

  }

  def main(args: Array[String]): Unit = {


    val s = flip(readInputFile())
              .map( s => s.groupBy(_.toChar)
                          .mapValues(_.size)
                          .toList.sortBy(r => r._2)
                          .head._1)
              .mkString

    println(s)

  }

}
