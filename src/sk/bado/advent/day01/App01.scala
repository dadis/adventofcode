package sk.bado.advent.day01

import sk.bado.advent.day01.Direction.Direction
import sk.bado.advent.day01.Geo.Geo

import scala.io.Source


object Direction extends Enumeration {
  type Direction = Value
  val LEFT = Value("L")
  val RIGHT = Value("R")

  def valueOf(c: Char): Direction = {

    c match {
      case 'L' => LEFT
      case 'R' => RIGHT
    }
  }
}

object Geo extends Enumeration {
  type Geo = Value
  val NORTH = Value("N")
  val SOUTH = Value("S")
  val WEST = Value("W")
  val EAST = Value("E")

  def valueOf(c: Char): Geo = {

    c match {
      case 'N' => NORTH
      case 'S' => SOUTH
      case 'E' => EAST
      case 'W' => WEST
    }
  }
}

object App01 {

  case class Step(direction: Direction, noOfSteps: Int) {}

  case class Position(x: Int, y: Int, direction: Geo)

  case class SimplePosition(x: Int, y: Int)

  def toStep(string: String): Step = {

    val direction = Direction.valueOf(string.charAt(0))
    val noOfSteps = Integer.parseInt(string.substring(1))
    new Step(direction, noOfSteps)
  }

  def readInputFile(): List[Step] = {


    Source.fromFile("./input01.txt")
      .getLines
      .flatMap(line => line.split(", "))
      .map(string => toStep(string))
      .toList
  }

  def decideNewDirection(startDirection: Geo, turnDirection: Direction): Geo = {

    if (startDirection == Geo.NORTH && turnDirection == Direction.LEFT) {
      Geo.WEST
    }

    else if (startDirection == Geo.NORTH && turnDirection == Direction.RIGHT) {
      Geo.EAST
    }

    else if (startDirection == Geo.EAST && turnDirection == Direction.LEFT) {
      Geo.NORTH
    }

    else if (startDirection == Geo.EAST && turnDirection == Direction.RIGHT) {
      Geo.SOUTH
    }

    else if (startDirection == Geo.SOUTH && turnDirection == Direction.LEFT) {
      Geo.EAST
    }

    else if (startDirection == Geo.SOUTH && turnDirection == Direction.RIGHT) {
      Geo.WEST
    }

    else if (startDirection == Geo.WEST && turnDirection == Direction.LEFT) {
      Geo.SOUTH
    }

    else if (startDirection == Geo.WEST && turnDirection == Direction.RIGHT) {
      Geo.NORTH
    }
    else Geo.NORTH


  }

  def makeAMove(start: Position, step: Step): Position = {

    val newDirection = decideNewDirection(start.direction, step.direction)

    newDirection match {
      case Geo.NORTH => Position(start.x, start.y + step.noOfSteps, newDirection)
      case Geo.SOUTH => Position(start.x, start.y - step.noOfSteps, newDirection)
      case Geo.EAST => Position(start.x + step.noOfSteps, start.y, newDirection)
      case Geo.WEST => Position(start.x - step.noOfSteps, start.y, newDirection)
    }

  }

  def pointOnY(startX: Int, startY: Int, endY: Int): scala.List[SimplePosition] = {


    val rang = {

      if(startY < endY){
        (startY to endY)
      }
      else {
        (startY to endY by -1)
      }
    }
    rang.map(y => new SimplePosition(startX, y)).toList.tail

  }

  def pointOnX(startX: Int, endX: Int, startY: Int): scala.List[SimplePosition] = {

    val rang = {

      if(startX < endX){
        (startX to endX)
      }
      else {
        (startX to endX by -1)
      }
    }
    rang.map(x => new SimplePosition(x, startY)).toList.tail
  }

  def getAllPointsBetween(start: Position, step: Step): List[SimplePosition] = {

    val newDirection = decideNewDirection(start.direction, step.direction)

    newDirection match {
      case Geo.NORTH => pointOnY(start.x, start.y, start.y + step.noOfSteps)
      case Geo.SOUTH => pointOnY(start.x, start.y, start.y - step.noOfSteps)
      case Geo.EAST => pointOnX(start.x, start.x + step.noOfSteps, start.y)
      case Geo.WEST => pointOnX(start.x, start.x - step.noOfSteps, start.y)
    }

  }

  def goThroughAllSteps(start: Position, steps: List[Step]): SimplePosition = {

    if (steps.isEmpty) {
      return new SimplePosition(start.x,start.y);
    }

    val newPosition = makeAMove(start, steps.head)


    goThroughAllSteps(newPosition, steps.tail)

  }

  def findFirstCrossing(start: Position, steps: List[Step], visited: List[SimplePosition]):SimplePosition = {
    if (steps.isEmpty) {
      return new SimplePosition(start.x,start.y);
    }

    val newPosition = makeAMove(start, steps.head)
    val newVisited = getAllPointsBetween(start, steps.head)


    for (position <- newVisited) {
      if (visited.contains(position)) {
        return position
      }
    }


    findFirstCrossing(newPosition, steps.tail, newVisited ::: visited)

  }

  def getDistance(position:SimplePosition):Int = {
    Math.abs(position.x)+Math.abs(position.y);
  }

  def main(args: Array[String]): Unit = {


    val start = Position(0, 0, Geo.NORTH)
    val visited = List()

    val steps1 = List(Step(Direction.RIGHT, 2), Step(Direction.LEFT, 3));
    val steps2 = List(Step(Direction.RIGHT, 2), Step(Direction.RIGHT, 2), Step(Direction.RIGHT, 2));
    val steps3 = List(Step(Direction.RIGHT, 5), Step(Direction.LEFT, 5), Step(Direction.RIGHT, 5), Step(Direction.RIGHT, 3));
    val steps4 = List(Step(Direction.RIGHT, 8), Step(Direction.RIGHT, 4), Step(Direction.RIGHT, 4), Step(Direction.RIGHT, 8));


        val result1 = goThroughAllSteps(start,readInputFile())
        val result2 = findFirstCrossing(start,readInputFile(),visited)

        println("===================================")
        println("Result1 " + getDistance(result1))
        println("Result2 " + getDistance(result1))
        println()


  }

}
