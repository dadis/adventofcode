package sk.bado.advent.day02

import scala.io.Source


case class Position(x: Int, y: Int) {

  def valueFromPad(): Char = {

    this match {
      case Position(2, 0) => 'D'
      case Position(1, 1) => 'A'
      case Position(2, 1) => 'B'
      case Position(3, 1) => 'C'
      case Position(0, 2) => '5'
      case Position(1, 2) => '6'
      case Position(2, 2) => '7'
      case Position(3, 2) => '8'
      case Position(4, 2) => '9'
      case Position(1, 3) => '2'
      case Position(2, 3) => '3'
      case Position(3, 3) => '4'
      case Position(2, 4) => '1'
    }

  }

  def moveUp(): Position = {

    if ((x == 0 && y == 2) ||
      (x == 1 && y == 3) ||
      (x == 2 && y == 4) ||
      (x == 3 && y == 3) ||
      (x == 4 && y == 2)
    ) {
      Position(x, y)
    }
    else {
      Position(x, y + 1)
    }

  }

  def moveDown(): Position = {

    if ((x == 0 && y == 2) ||
      (x == 1 && y == 1) ||
      (x == 2 && y == 0) ||
      (x == 3 && y == 1) ||
      (x == 4 && y == 2)
    ) {
      Position(x, y)
    }
    else {
      Position(x, y - 1)
    }
  }

  def moveLeft(): Position = {

    if ((x == 2 && y == 4) ||
      (x == 1 && y == 3) ||
      (x == 0 && y == 2) ||
      (x == 1 && y == 1) ||
      (x == 2 && y == 0)
    ) {
      Position(x, y)
    }
    else {
      Position(x - 1, y)
    }

  }

  def moveRight() = {

    if ((x == 2 && y == 4) ||
      (x == 3 && y == 3) ||
      (x == 4 && y == 2) ||
      (x == 3 && y == 1) ||
      (x == 2 && y == 0)) {
      Position(x, y)
    }
    else {
      Position(x + 1, y)
    }
  }

}

object App02 {

  type Sequence = List[Char]

  def readInputFile(): List[Sequence] = {


    Source.fromFile("./input02.txt")
      .getLines
      .map(line => line.toList)
      .toList
  }

  def makeAMove(position: Position, step: Char) = {

    step match {
      case 'U' => position.moveUp()
      case 'D' => position.moveDown()
      case 'L' => position.moveLeft()
      case 'R' => position.moveRight()
    }

  }

  def goThrougSequence(start: Position, sequence: Sequence): Position = {

    if (sequence.isEmpty) {
      return start;
    }

    val newPosition = makeAMove(start, sequence.head)


    goThrougSequence(newPosition, sequence.tail)

  }

  def runTest() = {
    val pad = Position(1, 1)
    val input = readInputFile()

    println("Start: " + pad.valueFromPad)

    val p1 = goThrougSequence(pad, input(0))
    val p2 = goThrougSequence(p1, input(1))
    val p3 = goThrougSequence(p2, input(2))
    val p4 = goThrougSequence(p3, input(3))


    println(p1.valueFromPad())
    println(p2.valueFromPad())
    println(p3.valueFromPad())
    println(p4.valueFromPad())
  }

  def solve(start: Position, instructions: List[Sequence]): List[Char] = {

    if (instructions.isEmpty) {
      return List()
    }

    val currentPosition = goThrougSequence(start, instructions.head)

    currentPosition.valueFromPad() :: solve(currentPosition, instructions.tail)


  }


  def main(args: Array[String]): Unit = {

    val start = Position(0, 2)
    val result = solve(start, readInputFile())

    println(result)


  }

}
