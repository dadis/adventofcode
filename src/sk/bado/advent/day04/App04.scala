package sk.bado.advent.day04

import scala.io.Source

object App04 {

  case class Room(name: String, decypheredName:String,id : Int, checkSum:String){

    def isReal():Boolean ={

      val counted =  name.toList
        .groupBy(c => c)
        .mapValues(_.size)
        .toList
        .sortBy{ r => (-r._2,r._1) }
        .take(5).map( _._1) mkString

      counted == checkSum
    }
  }

  def toRoom(line: String): Room = {

    val first = line.split('[')(0).replace("-","")
    val name = first.replaceAll("[0-9]","")
    val id = Integer.parseInt(first.takeRight(3))

    val checkSum = line.split('[')(1).dropRight(1)

    val decypheredName = decypher(name,id)

    Room(name,decypheredName,id,checkSum)
  }

  def decypher(text:String, id:Int):String = {
    val alphabet = "abcdefghijklmnopqrstuvwxyz"

    val shift = (id + alphabet.size) % alphabet.size

    val outputText = text.map((c: Char) => {

      val x = alphabet.indexOf(c.toLower)

      if (x == -1) {
        c
      }
      else {
        alphabet((x + shift) % alphabet.size)
      }
    });

    return  outputText
  }

  def readInputFile(): List[Room] = {


    Source.fromFile("./input04.txt")
      .getLines
      .map(line => toRoom(line))
      .toList
  }

  def main(args: Array[String]): Unit = {

    val sum = readInputFile().filter(_.isReal()).filter(_.decypheredName.contains("northpole"))

    println(sum.head.id)


  }


}

